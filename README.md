Para executar esse programa no linux, baixe o repósitorio. 
Depois, já dentro de sua pasta, utilize o comando "gcc transf.c -o transf.bin -lpthread". 
Necessário especificar a biblioteca pthread para funcionar e ter o gcc instalado.

O projeto de SO consiste na resolução de um problema de concorrência. Para resolve-lo, foi implementado o uso de threads. Threads não são a única solução para o problema da concorrência, mas foi escolhida devido a sua rapidez.

Para gerenciar a threads, foi necessário incluir a biblioteca semaphore. O semáforo é uma váriavel protegida que gerencia o acesso a recursos compartilhados.

Após a execução do programa, é possível ver que as threads foram capazes de gerenciar as rápidas transferências entre contas, sem apresentar nenhum erro.
