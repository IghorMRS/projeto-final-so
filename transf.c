#define _GNU_SOURCE
#include <stdlib.h>
#include <malloc.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include <sched.h>
#include <stdio.h>
#include <stdbool.h>
#include <pthread.h>
#include <semaphore.h>


struct c {
	int saldo;
};

//declarando semáforo
sem_t semTransf;

bool invContas;

//número para identificar quantas transferências foram realizadas
int numTransf = 0;
typedef struct c conta;

conta from, to;
int valor;
//The child thread will execute this function
void* transferencia(void *arg){

	//realiza o bloqueio do semáforo
	sem_wait(&semTransf);

	numTransf++;
	printf("Transferencia n° %d \n" , numTransf);

		//necessário declarar o random para inverter as contas conforme o programa funciona
		invContas = rand () & 1;
		if(invContas == false){
		if (from.saldo >= valor){
			from.saldo -= valor;
			to.saldo += valor;
		}
	}else{
		if (to.saldo >= valor){
			to.saldo -= valor;
			from.saldo += valor;
		}	
	}

	printf("******************************************\n");
	printf("|Transferência concluída com sucesso!|\n");
	printf("\n Saldo de c1:    %d\n", from.saldo);
	printf("\n Saldo de c2:    %d\n", to.saldo);
	printf("******************************************\n");

	//desbloqueia o semáforo
	sem_post(&semTransf);
}	

int main(){
	void* stack; 
	int i;
	int op;
	bool exit;
	//id da thread
	pthread_t vthread[100];
	//inicializa o semáforo para começar a interagir com as threads.
	sem_init(&semTransf, 0 , 1);

	// Todas as contas começam com saldo 100
	from.saldo = 100;
	to.saldo = 100;
	valor = 1;

	while (!exit) {

	printf("Selecione uma opção:\n");
	printf("1-Executa as transferencias\n");
	//printf("2-Troca as contas from e to\n");
	printf("3-Sair do Programa\n");
	op = getchar();
	getchar();


	
	switch (op)

	{
		case ('1') :
		for(i = 0; i < 100; i++){
			//cria uma nova thread
			pthread_create(&vthread[i], NULL, *transferencia, NULL);
	}

	
		for(i = 0; i < 100; i++){
			//suspende a execução da thread até que a thread anterior termine
			pthread_join(vthread[i], NULL);
			printf("Thread finalizada: %ld\n", vthread[i]);
		}


		printf("Transferencias realizadas: %d\n", numTransf);

		break;

		/*case ('2') :

		printf("Troca de contas realizada\n");

		break;*/

		case ('3'):
			exit = true;

		break;

		default : 
		printf("Escolha uma opção apropriada!\n");
		printf("******************************************\n");

		break;
	}

}
	return 0;
}
